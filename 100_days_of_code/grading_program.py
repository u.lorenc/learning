student_scores = {
    "Harry": 81,
    "Ron": 78,
    "Hermione": 99,
    "Draco": 74,
    "Neville": 62,
}

student_grades = {}
for key, value in student_scores.items():
    if value >= 91:
        grade = "Outstanding"
    elif 81 <= value <= 90:
        grade = "Exceeds Expectations"
    elif 71 <= value <= 80:
        grade = "Acceptable"
    else:
        grade = "Fail"
    student_grades[key] = grade


print(student_grades)

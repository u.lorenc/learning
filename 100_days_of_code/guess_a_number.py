# Day 12 - Number Guessing Game

# to do
# choosing a random number between 1 and 100
# make a function to set difficulty
# let the user guess a number
# function to check user's guess against actual answer
# track the number of turns and reduce by 1 if they get it wrong
# repeat the guessing functionality if they get it wrong


from random import randint
from guess_agme_art import logo
EASY_LEVEL_TURNS = 10
HARD_LEVEL_TURNS = 5


# function to check user's guess against actual answer
def check_answer(guess, answer, turns):
    """ CHECKS ANSWER AGAINST GUESS. RETURNS THE NUMBER OF TURNS REMAINING."""
    if guess > answer:
        print("Too high.")
        return turns - 1
    elif guess < answer:
        print("Too low.")
        return turns - 1
    else:
        print(f"Your got it! The answer is: {answer}.")


# make a function to set difficulty
def set_difficulty():
    level = input("Choose a difficulty. Type 'easy' or 'hard': ")
    if level == "easy":
        return EASY_LEVEL_TURNS
    else:
        return HARD_LEVEL_TURNS


def game():
    print(logo)
    # choosing a random number between 1 and 100
    print("Welcome to the guessing game!")
    print("I'm thinking of a number between 1 and 100.")
    answer = randint(1, 100)
    # print(f"the correct answer is {answer}")

    turns = set_difficulty()

    # repeat the guessing functionality if they get it wrong
    guess = 0
    while guess != answer:
        print(f"You have {turns} attempts remaining to guess the number.")
        # let the user guess a number
        guess = int(input("Make a guess: "))
        # track the number of turns and reduce by 1 if they get it wrong
        turns = check_answer(guess, answer, turns)
        if turns == 0:
            print("You've run out of guesses, you lose.")
            return
        elif guess != answer:
            print("Guess again.")


game()

# Treasure island game
#
# * 'You\'re at a crossroad. Where do you want to go? Type "left" or "right"'
# * 'You\'ve come to a lake. There is an island in the middle of the lake. Type "wait" to wait for a boat. Type "swim"
# to swim across.'
# * "You arrive at the island unharmed. There is a house with 3 doors. One red, one yellow and one blue. Which
# colour do you choose?"
# * "It's a room full of fire. Game Over."
# * "You found the treasure! You Win!"
# * "You enter a room of beasts. Game Over."
# * "You chose a door that doesn't exist. Game Over."
# * "You get attacked by an angry trout. Game Over."
# * "You fell into a hole. Game Over."


def display_intro():
    print('''Welcome to Treasure Island. Your mission is to find the treasure.''')


print()


def choose_path():
    print("You\'re at a crossroad. Where do you want to go?")
    crossroad = input('Type "left" or "right" ').lower()
    if crossroad == "left":
        print("You are going in the right direction.")
        return True
    else:
        print("You fell off the cliff. Game Over.")
        return False


def choose_crossing_lake():
    print("You\'ve come to a lake. There is an island in the middle of the lake.")
    lake = input('Type "wait" to wait for a boat. Type "swim" to swim across. ').lower()
    if lake == "wait":
        print("Please wait for boat it will arrive shortly.")
        return True
    else:
        print("Oh no! You get attacked by an angry trout. Game Over.")
        return False


def choose_door():
    print("You arrive at the island unharmed. There is a house with 3 doors. One red, one yellow and one blue.")
    door = input("Which colour do you choose? ").lower()
    if door == "blue":
        print("You found the treasure! You Win!")
    elif door == "yellow" or door == "red":
        print("You enter a room of beasts. Game Over.")
    else:
        print("You chose a door that doesn't exist. Game Over.")


play_again = "yes"
while play_again == "yes" or play_again == "y":
    display_intro()
    is_correct_path = choose_path()
    if is_correct_path:
        is_correct_lake_crossing = choose_crossing_lake()
        if is_correct_lake_crossing:
            choose_door()

    print("Do you want play again? (yes or no)")
    play_again = input().lower()

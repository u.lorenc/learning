# Day 5
# Password Generator Project

import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
           'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
           'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the PyPassword Generator!")

nr_letters = int(input("How many letters would you like in your password?\n"))
nr_symbols = int(input(f"How many symbols would you like?\n"))
nr_numbers = int(input(f"How many numbers would you like?\n"))

# Eazy Level - Order not randomised:
# e.g. 4 letter, 2 symbol, 2 number = JduE&!91
random_password = []

while nr_letters > 0:
    random_password.append(letters[random.randint(0, len(letters) - 1)])
    nr_letters -= 1

while nr_symbols > 0:
    random_password.append(symbols[random.randint(0, len(symbols) - 1)])
    nr_symbols -= 1

while nr_numbers > 0:
    random_password.append(numbers[random.randint(0, len(numbers) - 1)])
    nr_numbers -= 1

print(''.join(random_password))

# hard Level - Order of characters randomised:
# e.g. 4 letter, 2 symbol, 2 number = g^2jk8&P

nr_letters_2 = int(input("How many letters would you like in your password?\n"))
nr_symbols_2 = int(input(f"How many symbols would you like?\n"))
nr_numbers_2 = int(input(f"How many numbers would you like?\n"))

random_password_2 = []

while nr_letters_2 > 0:
    random_password_2.append(letters[random.randint(0, len(letters) - 1)])
    nr_letters_2 -= 1

while nr_symbols_2 > 0:
    random_password_2.append(symbols[random.randint(0, len(symbols) - 1)])
    nr_symbols_2 -= 1

while nr_numbers_2 > 0:
    random_password_2.append(numbers[random.randint(0, len(numbers) - 1)])
    nr_numbers_2 -= 1

random.shuffle(random_password_2)
print(''.join(random_password_2))

# ver. 3

scrambled_password = []

while len(random_password) > 0:
    rnd_idx = random.randint(0, len(random_password) - 1)
    scrambled_password.append(random_password[rnd_idx])
    temp_list = []
    for idx in range(0, len(random_password)):
        if rnd_idx != idx:
            temp_list.append(random_password[idx])

    random_password = temp_list
    print(''.join(random_password))
    print(''.join(scrambled_password))
    print('')

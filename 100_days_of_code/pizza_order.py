# Congratulations, you've got a job at Python Pizza. Your first job is to build an automatic pizza order program.
# Based on a user's order, work out their final bill.
# Small Pizza: $15
# Medium Pizza: $20
# Large Pizza: $25
# Pepperoni for Small Pizza: +$2
# Pepperoni for Medium or Large Pizza: +$3
# Extra cheese for any size pizza: + $1

print("Welcome to Python Pizza Deliveries!")
size = input("What size pizza do you want? S, M, or L ").upper()
add_pepperoni = input("Do you want pepperoni? Y or N ").upper()
extra_cheese = input("Do you want extra cheese? Y or N ").upper()


def pizza_order():
    pizza_price = 0
    if size == "S":
        pizza_price += 15
    elif size == "M":
        pizza_price += 20
    else:
        pizza_price += 25

    if add_pepperoni == "Y":
        if size == "S":
            pizza_price += 2
        else:
            pizza_price += 3

    if extra_cheese == "Y":
        pizza_price += 1

    print(f"Your final bill is: ${pizza_price}.")


pizza_order()

# logical operators (and, or, not)

print((9 > 7) and (2 < 4))   # both original expressions are True
print((8 == 8) or (6 != 6))  # one original expression is TRue
print(not(3 <= 1))           # original expression is False

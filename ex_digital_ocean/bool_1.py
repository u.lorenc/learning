x = 24
y = 10


print("x == y:", x == y) # equal
print("x != y:", x != y) # not equal
print("x < y:", x < y) # less than
print("x > y:", x > y) # greater than
print("x <= y:", x <= y) # less tha or equal to
print("x >= y:", x >= y) # greater than or equal to


# to better understand this output = we'll have Python also print a string to
# show us what it's evaluating


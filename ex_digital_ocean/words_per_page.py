# divmod() first returning the quotient that comes from floor division, the the remainder

words = 80000 # how many words in our book
per_page_A = 300 # option a - 300 words per page
per_page_B = 250 # option b - 250 words per page


print(divmod(words, per_page_A)) # calculate opt a
print(divmod(words, per_page_B)) # calculate opt b


# opt A - 266 pages, 200 words left over for total of 267 pages

# opt B 320 page book


a = 985.5
b = 115.25


print(divmod(a, b))

# verify what divmod() did
print(a//b) # floor division
print(a%b)  # modulo operator

# sum list

some_floats = [1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9]
print(sum(some_floats))


# sum tuples and dictionaries

print(sum((8, 16, 64, 512)))        # tuple
print(sum({-10: 'x', -20: 'y', -30: 'z'}))      # dictionary

print(sum(some_floats, 0.5))
print(sum({-10: 'x', -20: 'y', -30: 'z'}, 60))
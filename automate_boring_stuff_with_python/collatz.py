# funkcja collatz() ma mieć jeden parametr number
# spr czy number jest even
# if number % 2 == 0 (even)
# jeśli tak to print number //2 i zwrócić jej wartość
# jeśli nie (odd) to print i zwrócić 3* number + 1
# if number % 2 == 1 (odd)
# f. ma dać wprowadzić dane (input) dopóki  nie zwróci wartości 1 - int()
# try except = jeśli zostanie wprowadzone słowo zamiast cyfr
# message - must enter a number


def collatz(number):
    if number % 2 == 0:      # even number
        return number // 2

    elif number % 2 == 1:      # odd number
        return number * 3 + 1


print('Please enter a number')  # ask for the number


# check if the number is an integer, if so, see if even or odd. If not, rebuke and exit

try:
    number = int(input())
    while number !=1:
        collatz(number)
        print(number)
        number = collatz(number)
    else:
        print('You Win. The number is now 1!')
except ValueError:
    print('Please enter an integer')

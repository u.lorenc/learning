def top_three(input_list):
    """Returns a list of the three largest elements input_list in order from largest to smallest.

    If input_list has fewer than three elements, return input_list element sorted largest to smallest/
    """
    # TODO: implement this function
    
    answer = []
    
    for _ in range(3):
        if len(input_list):
            max_item=max(input_list)
            answer.append(max_item)
            input_list.remove(max_item)
    
    
    
    return answer





def top_three(input_list):
    return sorted(input_list, reverse=True)[:3]

def top_three(input_list):
    input_list = sorted(input_list, reverse=True)
    return input_list[:3]

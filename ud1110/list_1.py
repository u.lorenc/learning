#python_versions = [1.0, 1.5, 1.6, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6]

# python_versions[0]
#1.0

#python_versions[1]
#1.5

#python_versions[7]
#2.4

#Complete this function, how_many_days, that takes as its input a number 
#representing the month, and returns how many days are in that month. 
#The days_in_month function that we've defined for you is a list of how 
#many days are in each month. For example, days_in_month(8) should return 
#31 because the eighth month, August, has 31 days.


#def how_many_days(month_number):
  #  """Returns the number of days in a month.
   # WARNING: This function doesn't account for leap years!
    #"""
 
def days_in_month(month):
	days_in_month = [31,28,31,30,31,30,31,31,30,31,30,31]
    #todo: return the correct value

	return[month -1]
	  
# This test case should print 31, the number of days in the eighth month, August
print(days_in_month(8))


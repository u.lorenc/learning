#prize for points

#wooden_rabbit = {0,50}
#no_prize = {51,150}
#wafer_thin_mint = {151,180}
#penguin = {181,200}


def which_prize(points):
	"""
    Returns the prize-winning message, given a number of points
    """

	if points <= 50:
		return "Congratulation! You have won a wooden rabbit!"
	elif points <= 150:
		return "Oh dear, no prize this time."
	elif points <= 180:
		return "Congratulation! You have won a wafer thin mint!" 
	else:
		return "Congratulation! You have won a penguin!"

#Start your function by setting the variable prize = None, 
#change the prize depending on the number of points and then use 
#another conditional to return a message depending on whether prize 
#is there or not. This will avoid repeating the return part of the code.

def which_prize(points):
    """
    Returns the number of prize-winning message, given a number of points
    """
    prize = None
    if points <= 50:
        prize = "a wooden rabbit"
    elif 151 <= points <= 180:
        prize = "a wafer-thin mint"
    elif points >= 181:
        prize = "a penguin"

    if prize:
        return "Congratulations! You have won " + prize + "!"
    else:
        return "Oh dear, no prize this time."

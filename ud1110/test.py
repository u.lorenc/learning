#!/usr/bin/python3

class Dziewczyna:
	def __init__(self, imie):
		self.imie = imie
		
	def przedstaw_sie(self):
		print("Jestem %s." % self.imie)
		
	def say(self, text):
		try:
			print(text)
		except:
			print("I can't say that!")


Ulcia = Dziewczyna("Ulcia")
Ulcia.przedstaw_sie()
Ulcia.say("hello my big python")


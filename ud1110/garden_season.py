season = "winter"

def garden_calendar(season):
	if season == "spring":
		print("time to plant the garden!")
	elif season == "summer":
		print("time to water plants!")
	elif season == "fall" or season == "autumn":
		print ("time to harvest the garden!")
	elif season == "winter":
		print ("time to stay indoors and drink tea!")
	else:
		print("I don't recognize season!")


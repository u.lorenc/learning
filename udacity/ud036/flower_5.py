import turtle


def draw_turtleshapes():
    window = turtle.Screen()
    window.bgcolor("red")

    draw_flower()

    window.exitonclick()


def draw_flower():
    flora = turtle.Turtle()
    flora.penup()
    flora.setposition(-200, 200)
    flora.pendown()

    flora.shape("turtle")
    flora.color("green")

    flora.speed(50)
    flora.pensize(2)
    flora.seth(225)

# draw the petals

    for j in range(37):
        for i in range(3):
            flora.forward(200)
            flora.right(-120)
            if i == 0:
                flora.stamp()
        flora.left(10)
        flora.forward(20)

# draw the stem

    flora.penup()
    flora.setposition(-111, 57)
    flora.pendown()

    flora.color("brown")
    flora.seth(270)
    flora.pensize(2)
    flora.pensize(12)
    flora.forward(500)


draw_turtleshapes()


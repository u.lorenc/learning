import turtle


def draw_art():
    window = turtle.Screen()
    window.bgcolor("green")


# create the turtle Angie which draw a circular fractal

    angie = turtle.Turtle()
    angie.shape("arrow")
    angie.color("blue")
    angie.speed(15)

    for i in range(1, 37):
        angie.right(10)
        angie.circle(100)

    window.exitonclick()


draw_art()
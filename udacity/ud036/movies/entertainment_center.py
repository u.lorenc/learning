import media
import fresh_tomatoes

toy_story = media.Movie("Toy Story",
                        "A story o a boy and his toys that come to life",
                        "https://en.wikipedia.org/wiki/Toy_Story#/media/File:Toy_Story.jpg",
                        "https://www.youtube.com/watch?v=KYz2wyBy3kc")
#print(toy_story.storyline)

avatar = media.Movie("Avatar",
                     "A marine on an alien planet",
                     "https://sco.wikipedia.org/wiki/File:Avatar-Teaser-Poster.jpg",
                     "https://www.youtube.com/watch?v=5PSNL1qE6VY")
#print(avatar.storyline)
#avatar.show_trailer()


coco = media.Movie("Coco",
                   "Aspiring musician Miguel, confronted with his family's ancestral ban on music, "
                   "enters the Land of the Dead to find his great-great-grandfather, a legendary singer.",
                   "https://www.openwriting.com/2018/01/coco-pixar-film-review-2017/coco-film-poster-2017/",
                   "https://www.youtube.com/watch?v=5sSMRg1X1vg")

#print(coco.storyline)
#coco.show_trailer()

school_of_rock = media.Movie("School o rock", "storyline",
                             "https://www.imdb.com/title/tt4129004/mediaviewer/rm1835336192",
                             "https://www.youtube.com/watch?v=3PsUJFEBC74")

ratatouille = media.Movie("Ratatouille", "storyline",
                          "https://www.youtube.com/watch?v=nYUjNQrokeg",
                          "https://www.imdb.com/title/tt0382932/mediaviewer/rm937921792")

midnight_in_paris = media.Movie("Midnight in Paris", "storyline",
                               "https://www.youtube.com/watch?v=FAfR8omt-CY",
                               "https://www.imdb.com/title/tt1605783/mediaviewer/rm2452601600")

movies = [toy_story, avatar, coco, school_of_rock, ratatouille, midnight_in_paris]
fresh_tomatoes.open_movies_page(movies)
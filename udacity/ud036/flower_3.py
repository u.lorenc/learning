import turtle


def draw_square(some_turtle):

    for i in range(4):
        some_turtle.forward(200)
        some_turtle.right(90)


def draw_art():

    window = turtle.Screen()
    window.bgcolor("lightblue")

    brad = turtle.Turtle()
    brad.shape("circle")
    brad.speed("fastest")
    brad.shapesize(1, 1)
    brad.color("blue", "purple")
    brad.begin_fill()

    for i in range(72):
        draw_square(brad)
        brad.right(5)

    brad.end_fill()
    window.exitonclick()


draw_art()
# write code to check if the numbers provided in the list check_prime are prime numbers.
# If the numbers are prime, the code should print "[number] is a prime number."
# If the number is NOT a prime number, it should print "[number] is not a prime number", and a factor of that number,
# other than 1 and the number itself: "[factor] is a factor of [number]".

# HINT: You can use the modulo operator to find a factor

# prime numbers are greater than 1
# number is not prime if modulo is 0
# if check_prime > 1:
# check for factors

# iterate through the check_prime list
# search for factors, iterating through numbers ranging from 2 to the number itself
# otherwise keep checking until we've searched all possible factors, and then declare it prime


check_prime = [26, 39, 51, 53, 57, 79, 85]

for num in check_prime:
    for i in range(2, num):
        if num % i == 0:
            print("{} is NOT a prime number, because {} is a factor of {}".format(num, i, num))
            break
    if i == num - 1:
        print("{} IS a prime number".format(num))

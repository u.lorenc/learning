def exercise_1():
    # Use a list comprehension to create a new list first_names containing just the first names in names in lowercase

    names = ["Rick Sanchez", "Morty Smith", "Summer Smith", "Jerry Smith", "Beth Smith"]

    first_names = [name.split()[0].lower() for name in names]
    print(first_names)

def exercise_2():
    # Use a list comprehension to create a list multiples_3 containing the first 20 multiples of 3.

    multiples_3 = [x * 3 for x in range(1, 21)]
    print(len(multiples_3))
    print(multiples_3)

def exercise_3():
    # Use a list comprehension to create a list of names passed that only include those that scored at least 65.

    scores = {
        "Rick Sanchez": 70,
        "Morty Smith": 35,
        "Summer Smith": 82,
        "Jerry Smith": 23,
        "Beth Smith": 98
    }

    passed2 = []
    passed3 = []

    for name, score in scores.items():
        if score >= 65:
            passed2.append(name)

    for name in scores.keys():
       # score = scores.get(name)
        if scores.get(name) >= 65:
            passed3.append(name)

    passed = [name for name, score in scores.items() if score >= 65]
    print(passed)
    print(passed2)
    print(passed3)
exercise_3()

# Write a function named population_density that takes two arguments, population and land_area, and returns a
# population density calculated from those values.

def population_density(population, land_area):
    return population / land_area

# test cases for your function
test1 = population_density(10, 1)
expected_result1 = 10
print("expected result: {}, actual result: {}".format(expected_result1, test1))

test2 = population_density(864816, 121.4)
expected_result2 = 7123.6902801
print("expected result: {}, actual result: {}".format(expected_result2, test2))


# Write a function named readable_timedelta. The function should take one argument, an integer days, and return a
# string that says how many weeks and days that is.

def readable_timedelta(days):
    weeks = days // 7
    remainder = days % 7 # use % to get the number of days that remain
    return f"{weeks} week(s) and {remainder} day(s)."

# test your function
print(readable_timedelta(10))

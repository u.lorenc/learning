# ex 1

'''
You decide you want to play a game where you are hiding
a number from someone.  Store this number in a variable
called 'answer'.  Another user provides a number called
'guess'.  By comparing guess to answer, you inform the user
if their guess is too high or too low.

Fill in the conditionals below to inform the user about how
their guess compares to the answer.
'''
points = 174  # use this input to make your submission

# write your if statement here
if points >= 1 and points <= 50:
    result = "Congratulations! You won a wooden rabbit!"
elif points >= 51 and points <= 150:
    result = "Oh dear, no prize this time."
elif points >= 151 and points <= 180:
    result = "Congratulations! You won a wafer-thin mint!"
else:
    result = "Congratulations! You won a penguin!"

print(result)

# second version

points = 174  # use this input when submitting your answer

# set prize to default value of None
prize = None

# use the value of points to assign prize to the correct prize name
if points <= 50:
    prize = "wooden rabbit"
elif 151 <= points <= 180:
    prize = "wafer-thin mint"
elif points >= 181:
    prize = "penguin"

# use the truth value of prize to assign result to the correct message
if prize:
    result = "Congratulations! You won a {}!".format(prize)
else:
    result = "Oh dear, no prize this time."

print(result)

# ex. 2
'''
Depending on where an individual is from we need to tax them 
appropriately.  The states of CA, MN, and 
NY have taxes of 7.5%, 9.5%, and 8.9% respectively.
Use this information to take the amount of a purchase and 
the corresponding state to assure that they are taxed by the right
amount.
'''
state = "CA"
purchase_amount = 55

if state == "CA":
    tax_amount = .075
    total_cost = purchase_amount*(1+tax_amount)
    result = "Since you're from {}, your total cost is {}.".format(state, total_cost)

elif state == "MN":
    tax_amount = .095
    total_cost = purchase_amount*(1+tax_amount)
    result = "Since you're from {}, your total cost is {}.".format(state, total_cost)

elif state == "NY":
    tax_amount = .089
    total_cost = purchase_amount*(1+tax_amount)
    result = "Since you're from {}, your total cost is {}.".format(state, total_cost)

print(result)

#  Create a function that opens the flowers.txt, reads every line in it, and saves it as a dictionary. The main
#  (separate) function should take user input (user's first name and last name) and parse the user input to identify
#  the first letter of the first name. It should then use it to print the flower name with the same first letter (from
#  dictionary created in the first function).

# HINT: create a dictionary from flowers.txt
# HINT: create a function to ask for user's first and last name
# print the desired output

flower_name_dict = {}

with open('flowers.txt') as flowers:
    for line in flowers:
        (key, value) = line.split(': ', 1)
        flower_name_dict[key] = value.rstrip('\n')


user_name = input('Enter your first and last name: ')
user_name = user_name.capitalize()
first_letter_of_name = user_name[0]

your_flower = flower_name_dict.get(first_letter_of_name, 'No flower for you')
print(f'Unique flower name with the first letter: {your_flower}')

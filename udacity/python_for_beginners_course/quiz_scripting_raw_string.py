# Write a script that does the following:
#
# Ask for user input 3 times. Once for a list of names, once for a list of missing assignment counts, and once for a
# list of grades. Use this input to create lists for names, assignments, and grades.
# Use a loop to print the message for each student with the correct values. The potential grade is simply the current
# grade added to two times the number of missing assignments.

names = input("Enter names separated by comas: ").title().split(",")
assignments = input("Enter assignment counts separated by commas: ").split(",")
grades = input("Enter grades separated by commas: ").split(",")

# write a for loop that iterates through each set of names, assignments, and grades to print each student's message

for name, assignment, grade in zip(names, assignments, grades):
    grade = int(grade)
    assignment = int(assignment)
    message = f"Hi {name},\nThis is a reminder that you have {assignment} assignments left to \
    submit before you can graduate. You're current grade is {grade} and can increase \
    to {2 * assignment} if you submit all assignments before the due date.\n"
    print(message)

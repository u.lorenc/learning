# ex 1

# Suppose you want to count from some number start_num by another number count_by until you hit a final number end_num.
# Use break_num as the variable that you'll change each time through the loop. For simplicity, assume that end_num is
# always larger than start_num and count_by is always positive.
# Before the loop, what do you want to set break_num equal to? How do you want to change break_num each time through
# the loop? What condition will you use to see when it's time to stop looping?
# After the loop is done, print out break_num, showing the value that indicated it was time to stop looping.
# It is the case that break_num should be a number that is the first number larger than end_num.

# provide some start number
start_num = 3
# provide some end number that you stop when you hit
end_num = 70
# provide some number to count by
count_by = 2

# write a while loop that uses break_num as the ongoing number to check against end_num

break_num = start_num
while break_num < end_num:
    break_num += count_by

print(break_num)

# ex2

# Suppose you want to count from some number start_num by another number count_by until you hit a final number end_num,
# and calculate break_num the way you did in the last quiz.
# Now in addition, address what would happen if someone gives a start_num that is greater than end_num. If this is the
# case, set result to "Oops! Looks like your start value is greater than the end value. Please try again." Otherwise,
# set result to the value of break_num.

# provide some start number
start_num = 300
# provide some end number that you stop when you hit
end_num = 548
# provide some number to count by
count_by = 23

# write a condition to check that end_num is larger than start_num before looping
# write a while loop that uses break_num as the ongoing number to check against end_num


if start_num > end_num:
    result = "Oops! Looks like your start value is greater than the end value. Please try again."
else:
    break_num = start_num
    while break_num < end_num:
        break_num += count_by

    result = break_num

print(result)

# ex 3
# Write a while loop that finds the largest square number less than an integer limit and stores it in a variable
# nearest_square. A square number is the product of an integer multiplied by itself, for example 36 is a square number
# because it equals 6*6. For example, if limit is 40, your code should set the nearest_square to 36.

limit = 40
num = 0

# write your while loop here
while (num + 1) ** 2 < limit:
    num +=1
nearest_square = num ** 2

print(nearest_square)

# ex 4
# You need to write a loop that takes the numbers in a given list named num_list:
# Your code should add up the odd numbers in the list, but only up to the first 5 odd numbers together. If there are more
# than 5 odd numbers, you should stop at the fifth. If there are fewer than 5 odd numbers, add all of the odd numbers.

num_list = [422, 136, 524, 85, 96, 719, 85, 92, 10, 17, 312, 542, 87, 23, 86, 191, 116, 35, 173, 45, 149, 59, 84, 69, 113, 166]

count_odd = 0
list_sum = 0
i = 0
len_num_list = len(num_list)

while (count_odd < 5) and (i < len_num_list):
    if num_list[i] % 2 != 0:
        list_sum += num_list[i]
        count_odd += 1
    i += 1


# for i in range(0, len(num_list)):
#     if num_list[i] % 2 != 0:
#         count_odd += 1
#         list_sum += num_list[i]
#         if count_odd == 5:
#             break


print("The numbers of odd numbers added are: {}".format(count_odd))
print("The sum of the odd numbers added is: {}".format(list_sum))


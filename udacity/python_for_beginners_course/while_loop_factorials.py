# 1
# number to find the factorial of
number = 6

# start with our product equal to one
product = 1

# track the current number being multiplied
current = 1

# write your while loop
# multiply the product so far by the current number
# increment current with each iteration until it reaches number
# print the factorial of number

while current <= number:
    product = product * current
    current = current + 1

print(product)

# 2
# number to find the factorial of
number = 6

# start with our product equal to one
product = 1

# write your for loop here
# print the factorial of number

for i in range(1, number + 1):
    product = product * i

print(product)
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 23:34:50 2022

@author: Ula
"""
'''
 1) ask user to put a number between 1 and 5 inclusive, int, print as str value
'''

user_input = int(input('Enter a number between 1 and 5:>>> '))


if user_input == 1:
    print('one')
elif user_input == 2:
    print('two')
elif user_input == 3:
    print('three')
elif user_input == 4:
    print('four')
elif user_input == 5:
    print('five')
else:
    print('Please choose a number between 1 and 5.')

'''
2) repeat the previous task but this time the user will input a string and the
 code will output the integer value. Convert the string to lowercase
'''  

user_input = input('Enter a number between one and five:>>> ')
user_input.lower()

if user_input == 'one':
    print(1)
elif user_input == 'two':
    print(2)
elif user_input == 'three':
    print(3)
elif user_input == 'four':
    print(4)
elif user_input == 'five':
    print(5)
else:
    print('Please choose a number between one and five')
    
''' 
3) create a variable containing an integer between 1 and 10 inclusive. Ask the 
user to guess the number. If they guess too high or too low, tell them they not
won. Tell them they win if they guess the correct number
    
'''  
secret_number = 3
guess = input('Guess a number between 1 and 10:>>> ')

if guess.isdigit():
    guess = int(guess)
    if guess == secret_number:
        print('You guessed the correct number! You win!')
    elif guess > secret_number and guess <= 10:
        print('You guessed to high. Sorry you lose!')
    elif guess < secret_number and guess  >= 1:
        print('You guess to low. Sorry you lose!')
    else:
        print('Out of range')
else:
    print('That\'s not even a number! What are you playing at?! ')
    
'''
4) Ask the user to input their name. Check the length of the name. It it is 
    greater then 5 character long, write a message telling them how many
    character otherwise write a message saying the length of their name is secret
'''

user_name = input('Enter your name:>>> ')

if len(user_name) > 5:
    print(f'Your name has {len(user_name)} letters.')
else:
    print('I\'m not telling you the lenght of your name is secret')

'''
5) ask the user for two integers between 1 and 20. If they are both greater than
    15 return their product. If only one is greater than 15 return their sum,
    if neither are greater than 15 return zero.
'''

int_1 = int(input('Enter first number between 1 and 20:> '))
int_2 = int(input('Enter second number between 1 and 20:> '))

if int_1 > 15 and int_2 > 15:
    print(int_1 * int_2)
elif int_1 > 15 or int_2 > 15:
    print(int_1 + int_2)
else:
    print(0)
    

'''
6) ask the user for two integers, then swap the content of the variables.
    var_1 = 1 and var_2 = 2 initially, once the code has run var_1 should equal
    to 2 and var_2 equal to 1
'''

var_1 = int(input('Enter first an integer:>>> '))
var_2 = int(input('Enter second integer:>>> '))

print(f'Before swaping var_1 = {var_1} and var_2 = {var_2}.')
var_1, var_2 = var_2, var_1
print(f'After swaping var_1 = {var_1} and var_2 = {var_2}.')
    

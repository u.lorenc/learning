# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 14:38:43 2022

@author: Ula
"""

for i in range(10):
    print(i)
    
print()

for  i in range(10):
    print(i,end=' ')
    
for i in range(1,11):
    print(i,end=' ')
    
for i in range(0,101,4):
    print(i,end=' ')

for i in range(100,0,-1):
    print(i,end=', ')
    
word = 'Python'

for i in word:
    print(i)

for char in word:
    print(char)
    
x = 5

x = x + 1

print('x =',x)

x+=1

print('x =',x)

y = x

print('y =',y)

x += 1
print('x =',x)
print('y =',y)

data = [53,76,25,98,56,42,69,81]

data_1 = []

data[0]

data[2]

data[5]

data[-1]

data[0:5]

for num in data:
    print(num,end=' ')

print()

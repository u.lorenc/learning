# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 22:57:49 2022

@author: Ula
"""

word = 'summer'

guess = input('I\'m thinkig of a word, can you guess what it is? Hint \
it\'s a season.>>> ')
    
guess = guess.lower()

if guess == 'summer':
    print('Yes, it\'s Summer: well done!')
elif guess == 'winter':
    print('No, it\'s not Winter. Sorry!')
elif guess == 'spring':
    print('No, it\'s not Spring. Sorry!')
elif guess == 'autumn':
    print('No it\'s not Autumn. Sorry!')
else:
    print(guess.capitalize(),'is not a season!')

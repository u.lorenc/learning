'''Fizzbuzz'''

n = 100

for i in range(1,n+1):
    if i % 3 == 0 and i % 5 == 0:
        print(i, 'Fizzbuzz!!!')
    elif i % 5 == 0: 
        print(i, 'Buzz')
    elif i % 3 == 0:
        print(i, 'Fizz')
    else:
        print(i)
        

"""
Created on Tue Feb  8 14:38:43 2022

@author: Ula
"""

'''put a counter in while loop'''

count = 0 
class_names = []
name = input('Please enter name type n to stop:> ')
while name!= 'n':
    count +=1
    class_names.append(name)
    print(f'{name} has been added.')
    name = input('Next name?:> ')
    
print(f'There are {count} people in the class, they are {class_names}')

# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 14:38:43 2022

@author: Ula
"""

# '''Bubble sort'''

data = [53,76,25,98,56,42,69,81]
    
data_copy = data[:]
for i in range(len(data_copy)):
    for j in range(0,len(data_copy)-i-1):
        if data_copy[j] > data_copy[j+1]:
            data_copy[j],data_copy[j+1] = data_copy[j+1],data_copy[j]
            
print(data_copy)

print(sorted(data))

my_list = [34,76,58]

my_list.append(100)

print(my_list)

my_list.remove(34)

print(my_list.index(100))


my_list.extend([67,68,69])
print(my_list)

print(my_list.index(67))


n = 10

while n>0:
    print(n)
    n= n-1
    
user_input = int(input('Please enter ages of class memeber. Type -1 to end:>'))
ages=[]
while user_input > 0:
    ages.append(user_input)
    user_input = int(input('The next age:> '))
print('The ages are', ages)

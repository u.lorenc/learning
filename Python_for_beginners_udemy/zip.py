'''zip'''
capitals = {'France':'Paris', 'Spain':'Madrid', 'United Kingdom':'London',
            'India':'New Delhi', 'United States':'Washington DC', 'Italy':'Rome',
            'Denmark':'Copenhagen', 'Germany':'Berlin', 'Greece':'Athens',
            'Bulgaria':'Sofia', 'Ireland':'Dublin', 'Mexico':'Mexico City'}

my_list_1 = [1,2,3,4]
my_list_2 = ['a', 'b', 'c', 'd']

joined = list(zip(my_list_1,my_list_2)) # łączenie
print(f'The result of the zip function is {joined} it is of type{type(joined)} ')

i,j = zip(*joined) # może rozdzielić krotki,listy etc (upacking)

print(capitals.items())
print()
x,y = zip(*capitals.items())

# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 16:35:35 2022

@author: Ula
"""

'''1) aks user for two numbers between 1 and 100. Then count from them the
lower number to the higher nummber. Print the ruslt on the screen'''

num_1 = int(input('Enter first number between 1 and 100:> '))
num_2 = int(input('Enter second number between 1 and 100:> '))

while num_1 < 0 or num_2 < 0 or num_1 > 100 or num_2 > 100 or num_1 == num_2:
    print('Numbers must be different values between 1 and 100, try again')
    num_1 = int(input('Enter first number between 1 and 100:> '))
    num_2 = int(input('Enter second number between 1 and 100:> '))
    
if num_1 < num_2:
    for i in range(num_1, num_2+1):
        print(i,end=' ')
else:
    for i in range(num_2,num_1+1):
        print(i,end=' ')
        
'''2) ask the user to input a string and then print it out to the screen in 
reverse order - use a for loop'''

word = input('Please enter a string:> ')
reverse_string = ''
for char in word:
    reverse_string = char + reverse_string
    
print(reverse_string)

print(word[::-1])

'''3) ask the user for a number between 1 and 12 and then display a times table 
for the number'''

user_input = input('Please enter a number between 1 and 12:> ')

while (not user_input.isdigit()) or (int(user_input) < 1 or 
int(user_input) > 12):
    print('Must be an integer between 1 and 12')
    user_input = input('Please make selection:> ')
user_input = int(user_input)
print('==========================')
print()
print(f'This is the {user_input} times table')
print()
for i in range(1,13):
    print(f'{i} x {user_input} = {i*user_input}')
    
'''4) can you amend the solution to question 3 so that it just prints out all time
tables between 1 and 12?'''

for i in range(1,13):
    print('=======================')
    print()
    print(f'This is the {i} times table')
    print()
    for j in range(1,13):
        print(f'{j} x {i} = {j*i}')
        
        
'''5) ask the user to input a sequence of numbers. Then calculate the mean - 
sum divided by the number of observation 
and print the result.'''

user_input = input('Please enter a number type exit to stop:> ')
numbers = []
while user_input.lower() != 'exit':
    while not user_input.isdigit():
        print('That is not a number! Numbers only please:> ')
        user_input = input('Try again:> ')
    numbers.append(int(user_input))
    user_input = input('Please enter next number:> ')
total = 0
for number in numbers:
    total += number
    
print(f'Mean is {total/len(numbers)}')
print(sum(numbers)/len(numbers))

'''6) write code that will calculate 15 fractorial - is product of positive 
ints up to a given number e.g. 5 factorial 5x4x3x2x1'''

n = 15
fact = 1
for i in range(1,n+1):
    fact = fact * i
    
print(fact)

'''7) write code to calculate Fibonacci numbers. Create list containing 20 
Fibonacci numbers, (Fib numbers made by sum of preceeding two. Series starts
0 1 1 2 3 5 8 13 ...)'''

# Number of fib numbers required

n = 20
# set a and b to the first two numbers in the sequence
a = 0
b = 1

#list in which to store numbers
fib_nums = []

# use a for loop to create a sequence, repeat n times
for i in range(n):
    fib_nums.append(a)
    a,b = b,a+b

print(f'The first {n} Fibonacci numbers are, {fib_nums}')


'''8) can you draw this
    *****
    *
    ****
    *
    *
    *
    6 rows & 5 columns
'''

star = '*' # string

for i in range(1,7):    # this will gave us rows
    for j in range(1,6):        # this will gave us columns
        if i == 1 and j < 6:    # first row
            print(star,end='')
        elif i == 2 and j == 1: # second row
            print()
            print(star)
        elif i == 3 and j < 5:
            print(star,end='')
        elif i == 4 and j == 1:
            print()
            print(star)
        elif i == 5 and j == 1:
            print(star)
        elif i == 6 and j == 1:
            print(star)
        
 
''' 9) write some code that will determine all odd and even numbers between
1 and 100. Put the odds in a list named odd and the evens in a list named even'''

# numbers required
n = 100

# instantiate empty lists
evens = []
odds = []

for i in range(n+1):
    if not i % 2:       # not True - even
        evens.append(i)
    else:
        odds.append(i)
print(f'The evens are {evens}')
print(f'The odds are {odds}')

# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 18:34:04 2022

@author: Ula
"""

import math

print(math.pi)
print(math.cos(0))

import random

print(random.randint(1,100))

for i in range(100):
    print(random.randint(1,100),end=' ')

import webbrowser

from random import randint
print(randint(1,100))

capitals = {'France':'Paris', 'Spain':'Madrid', 'United Kingdom':'London',
            'India':'New Delhi', 'United States':'Washington DC', 'Italy':'Rome',
            'Denmark':'Copenhagen', 'Germany':'Berlin', 'Greece':'Athens',
            'Bulgaria':'Sofia', 'Ireland':'Dublin', 'Mexico':'Mexico City'}

capitals['Germany']
capitals.get('Germany')

capitals['South Korea'] = 'Seoul'

print(capitals)
print(capitals.items())

for country in capitals:
    print(country)
    
for country, city in capitals.items():
    print(f'The capital of {country}, is {city}')
    
print(capitals.keys())

print()
print(capitals.values())

print()

if 'France' in capitals:
    print('It contains France')
    
l = [1,2,3,4,5]

print(5 in l)


"""
Created on Sun Feb  6 18:34:23 2022

@author: Ula
"""

x = 5
y = 6

print('x=' ,x, 'y=',y)
print('Checking less than with \'<\':',x<y)
print('Checking greater than with \'>\':',x>y)

var_1 = 7
var_2 = 7

print('var_1 = ', var_1, 'var_2 = ', var_2)
print('Checking equality with \'==\':', var_1 == var_2)
print('Checking not equal with \'!=\':', var_1 != var_2)
print('Checking less than or equal with \'<=\':', var_1 <= var_2)
print('Checking greater than with \'>=\':', var_1 >= var_2 )

print('Demostrate that \'True\' is Bolean in python with \
      \'type(True)\':', type(True))
    

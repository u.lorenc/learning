# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 18:56:32 2022

@author: Ula
"""

x = 1

help(x)
dir(x)

y = [1,2,3]
help(y)
dir(y)

z = {'a':1}
help(z)
dir(z)

# object combine functions with data. Think of list

# class is the print of of an object

# inside the class there are attributes, which are the variable for the class

class Patient(object):
    '''Medical centre patient'''
    pass

# class variablea and instance variables:

class Patient(object):
    '''medical centre patient'''
    # __init__ (metoda) która tworzy zmienne, które zawierają wartosci
    def __init__(self,name,age): # argumenty
        self.name = name         # zmienne instance variables
        self.age = age
    
steve = Patient('Steven Hughes', 48)
abigail = Patient('Abigail Sandwick',32)

class Patient(object):
    '''medical centre patient'''
    status = 'patient'     # class variable
    
    def __init__(self,name,age):
        self.name = name
        self.age = age



steve = Patient('Steven Hughes',48)
abigail = Patient('Abigail Sandwick',32)
 

class Patient(object):
    ''' Medical centre patient'''
    
    status = 'patient'

    def __init__(self,name,age):
        self.name = name
        self.age = age
        
        
    def get_details(self):
        print(f'Patient record: {self.name}, {self.age} years.')

steve = Patient('Steven Hughes',48)
abigail = Patient('Abigail Sandwick',32)


class Patient(object):    #(object) built in templet from which other classes inherit
      ''' Medical centre patient'''
     
      status = 'patient'
     
      def __init__(self,name,age):
          self.name = name
          self.age = age
          self.conditions = []
         
      def get_details(self):
          print(f'Patient record: {self.name}, {self.age} years.' \
                f' Current information: {self.conditions}.')
         
      def add_info(self,information):
          self.conditions.append(information)
    

# steve = Patient('Steven Hughes',48)
# abigail = Patient('Abigail Sandwick',32)

class Infant(Patient): # dziedziczy z klasy Patient
    ''' Patient under 2 years'''
    
    def __init__(self,name,age):
        self.vaccinations = []
        super().__init__(name,age) # przeszukuje klasę nadrzędną odnosnie komendy init
        
    def add_vac(self,vaccine):
        self.vaccinations.append(vaccine)
        
    def get_details(self):
          print(f'Patient record: {self.name}, {self.age} years.' \
                f' Patient has had {self.vaccinations} vaccines.' \
                f' Current information: {self.conditions}.' \
                f'\n{self.name} IS AN INFANT, HAS HE HAD ALL HIS CHECKS?')
          
archie = Infant('Archie Fittleworth',0)      
archie.add_vac('MMR') 
         
class Patient(object):
      ''' 
          Attributes
          ----------
          name: Patient name
          age: Patient age
          conditions: Existing medical conditions
      '''
     
      status = 'patient'
     
      def __init__(self,name,age):
          self.name = name
          self.age = age
          self.conditions = []
         
      def get_details(self):
          print(f'Patient record: {self.name}, {self.age} years.' \
                f' Current information: {self.conditions}.')
         
      def add_info(self,information):
          self.conditions.append(information)
         

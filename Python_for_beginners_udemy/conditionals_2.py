
"""
Created on Mon Feb  7 21:51:45 2022

@author: Ula
"""

var_3, var_4, var_5 = 5, 20, 25

print('var_3 =', var_3, 'var_4 =', var_4, 'var_5 =', var_5, end ='\n\n')

print('var_4 and var_5 < 100?', var_4 < 100 and var_5 < 100)
print('var_4 and var_5 < 22?', var_4 < 22 and var_5 < 22)
print('var_4 or var_5 < 22?', var_4 < 22 or var_5 < 22, end = '\n\n')

print('not True is:', not True)
print('not False is:', not False,end = '\n\n')

print('not(var_4 and var_5 < 100?)', not(var_4  < 100 and var_5 < 100))
print('not(var_4 and var_5 < 22?)', not(var_4 < 22 and var_5 < 22))
print('not(var_4 or var_5 < 22?)', not(var_4 < 22 or var_5 < 22))

# # if statement

some_condition = True

if some_condition:
    print('The variable \'some_condition\' is True')
else:
    print('The variable \'some_condition\' is False')
    
# What should I wear?

temp = int(input('Please enter the temperature in Celsius. \
An integer between 0-40:>>> '))

if temp > 30:
    print('Wear shorts and sun cream!')
elif temp <= 30 and temp > 20:
    print('It\'s warm, but not shorts weather!')
elif temp <= 20 and temp > 10:
    print('You\'ll probably need a vest today!')
else:
    print('Too cold! Don\'t go out!')

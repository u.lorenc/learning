my_list = [[1,2,3],[4,5,6],[7,8,9]]

my_list[0]
my_list[1]
my_list[2]

my_list[1][1]

countries = {'France':{'Capital':'Paris','Language':'French'},'Spain':{'Capital':'Madrid','Language':'Spanish'},
            'United Kingdom':{'Capital':'London','Language':'English'},
            'United States':{'Capital':'Washington DC','Language':'English'},
            'Italy':{'Capital':'Rome','Language':'Italian'}
            }


countries['France']
for key,value in countries.items():
    print(key, value)
    
print()
for key,value in countries.items():
    print(f'{value["Capital"]} is the capital of {key}, they speak {value["Language"]}.')

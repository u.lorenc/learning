# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 18:18:12 2022

@author: Ula
"""

# Exercises

'''
Question 1
Create a function that will calculate the sum of two numbers. Call it sum_two.
'''

def sum_two(a,b):
    
    return a + b

print(f'The sum of 3 and 4 is {sum_two(3,4)}')


'''
Question 2
Write a function that performs multiplication of two arguments. By default the
function should multiply the first argument by 2. Call it multiply.
'''

def multiply(a,b=2):
    
    '''
    Returns the product of a and b; if b not given returns 2 * a.
    '''
    
    return a * b

print(f'Inputting 3 gives {multiply(3)}')
print(f'Inputting 3 and 5 gives {multiply(3,5)}')


def multiply(a,b):
    return (2*a) * b
print(f'The product of two number is {multiply(3,4)}')

'''
Question 3
Write a function to calculate a to the power of b. If b is not given
its default value should be 2. Call it power.
'''

def power(a,b=2):
    return a ** b
print(f'the power of 4 gives {power(4)}')
print(f'the power of 4 and 6 gives {power(4,6)}')


'''
Question 4
Create a new file called capitals.txt , store the names of five capital cities
in the file on the same line.
'''

f = open('capitals.txt','w')

f.write('London, ')
f.write('Paris, ')
f.write('Madrid, ')
f.write('Lisbon, ')
f.write('Rome, ')

f.close()

'''
Question 5
Write some code that requests the user to input another capital city.
Add that city to the list of cities in capitals. Then print the file to
the screen.
'''

user_input = input('Please write a capital city:> ')

file = open('capitals.txt','a')
file.write('\n'+ user_input)
file.close()

file = open('capitals.txt','r')
print(file.read())
file.close()


'''
Question 6
Write a function that will copy the contents of one file to a new file.
'''

def copy_file(infile,outfile):
    
    '''copies the content of infile to a new file, outfile.'''
    
# open both files
    with open(infile) as file_1:
        with open(outfile, "w") as file_2:
            file_2.write(file_1.read())


copy_file('capitals.txt','new_capitals.txt')




# Simulate the results of an election using a Monte Carlo simulation
# Suppose two candidates, Candidate A and Candidate B, are running
# for mayor in a city with three voting regions. The most recent polls
# show that Candidate A has the following chances for winning in each
# region:
# Region 1: 87 percent chance of winning .87
# Region 2: 65 percent chance of winning .65
# Region 3: 17 percent chance of winning .17
# Write a program that simulates the election ten thousand times and
# prints the percentage of times in which Candidate A wins

from random import random

num_times_A_wins = 0
num_times_B_wins = 0

num_trails = 10_000
for trail in range(0, num_trails):
    votes_for_A = 0
    votes_for_B = 0

    # determine who wins in region 1

    if random() < 0.87:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    # determine who wins in region 2:

    if random() < 0.65:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    # determine who wins in region 3:

    if random() < 0.17:
        votes_for_A = votes_for_A + 1
    else:
        votes_for_B = votes_for_B + 1

    # determine election outcome:
    if votes_for_A > votes_for_B:
        num_times_A_wins = num_times_A_wins + 1
    else:
        num_times_B_wins = num_times_B_wins + 1

print(f"probability A wins: {num_times_A_wins/num_trails}")
print(f"probability B wins: {num_times_B_wins/num_trails}")



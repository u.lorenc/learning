# ex 2
# Write a program that displays "I'm thinking of a number between 1
# and 10. Guess which one." Then use input() to get a number from
# the user. If the user inputs the number 3, then the program should
# display "You win!" For any other input, the program should display
# "You lose."

print("I'm thinking of a number between 1 and 10. Guess which one.")
your_num = int(input("Enter a number: "))

if your_num == 3:
    print("You win!")
else:
    print("You lose.")

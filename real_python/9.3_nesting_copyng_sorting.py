# ex 1 create tuple
# ex 2 Write a for loop that loops over data and prints the sum of each
# nested tuple. The output should look like this:
# Row 1 sum: 3   data[0][0] + data[0][1]
# Row 2 sum: 7
# ex 3 Create the list [4, 3, 2, 1] and assign it to the variable numbers.
# ex 4. Create a copy of the numbers list using the [:] slice notation.
# ex 5. Sort the numbers list in numerical order using .sort().

data = ((1, 2), (3, 4))

for i in range(len(data)):
    print(f"Row {i+1} sum: {data[i][0] + data[i][1]}")


numbers = [4, 3, 2, 1]
nums = numbers[:]
nums.sort()
print(nums)

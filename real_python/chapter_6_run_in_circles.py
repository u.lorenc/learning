# asks for positive number


num = float(input("Enter a positive number: "))


while num <= 0:
    print("That's not a positive number!")
    num = float(input("Enter a positive number: "))

# asks the user to input an amount produces and display how to split between 2,3,4 and 5 people


amount = float(input("Enter an amount: "))


for num_people in range(2,6):
    print(f"{num_people} people: ${amount / num_people:.2f} each")


# ex 1 for loop int(2,10)

for n in range(2, 10):
    print(n)


# ex 2 while int(2,10)

i = 1
while i <11:
    print(i)
    i = i + 1


# ex 3

def doubles(num):
    """Return the result of multiplying an input number by 2."""
    return num * 2

# Call doubles() to double the number 2 three times


my_num = 2
for i in range(0,3):
    my_num = doubles(my_num)
    print(my_num)



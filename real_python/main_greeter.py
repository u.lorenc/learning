# Create a module called main.py that imports greet() from greet.py
# and calls the function with the argument "Real Python".

import greeter

greeter.greet("Real Python")

# If the two players are playing basketball, then the player with the
# greatest score wins.
# If the two players are playing golf, then the player with the lowest
# score wins.
# In either sport, if the two scores are equal, then the game is a draw.

sport = input("Enter a sport name: ")
score_1 = int(input("Enter a first score: "))
score_2 = int(input("Enter a second score:"))

# 1
if score_1 == score_2:
    print("The game is a draw.")

if sport.lower() == "basketball":
    if score_1 > score_2:
        print("Player number 1 win.")
    else:
        print("Player number 2 win.")

# 2

elif sport.lower() == "golf":
    if score_1 < score_2:
        print("Player number 1 win.")
    else:
        print("Player number 2 win.")

# 3
else:
    print("Unknown sport")


# more dense version

sport = input("Enter a sport name: ")
score_1 = int(input("Enter a first score: "))
score_2 = int(input("Enter a second score:"))

sport = sport.lower()

if score_1 == score_2:
    print("The game is a draw.")
elif (sport == "basketball") or (sport == "golf"):
    p1_wins_bball = (sport == "basketball") and (score_1 > score_2)
    p1_wins_golf = (sport == "golf") and (score_1 < score_2)
    p1_wins = p1_wins_bball or p1_wins_golf
    if p1_wins:
        print("Player 1 win.")
    else:
        print("Player 2 win.")
else:
    print("Unknown sport.")


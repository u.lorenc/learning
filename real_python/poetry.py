
import random

nouns= ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
prepositions =  ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

def make_poem():
    """Create a randomly generated poem, returned as a multi-line string."""
    # Pull three nouns randomly
    n_1 = random.choice(nouns)
    n_2 = random.choice(nouns)
    n_3 = random.choice(nouns)
    # Make sure that all the nouns are different
    while n_1 == n_2:
        n_2 = random.choice(nouns)
    while n_1 == n_3 or n_2 == n_3:
        n_3 = random.choice(nouns)

    # pull three verbs randomly
    v_1 = random.choice(verbs)
    v_2 = random.choice(verbs)
    v_3 = random.choice(verbs)
    while v_1 == v_2:
        v_2 = random.choice(verbs)
    while v_1 == v_3 or v_2 == v_3:
        v_3 = random.choice(verbs)

    # pull three adjectives randomly
    adj_1 = random.choice(adjectives)
    adj_2 = random.choice(adjectives)
    adj_3 = random.choice(adjectives)
    while adj_1 == adj_2:
        adj_2 = random.choice(adjectives)
    while adj_1 == adj_3 or adj_2 == adj_3:
        adj_3 = random.choice(adjectives)

    # pull two prepositions randomly
    p_1 = random.choice(prepositions)
    p_2 = random.choice(prepositions)
    p_3 = random.choice(prepositions)
    while p_1 == p_2:
        p_2 = random.choice(prepositions)
    while p_1 == p_3 or p_2 == p_3:
        p_3 = random.choice(prepositions)

    # pull one adverb randomly
    adv_1 = random.choice(adverbs)

    if "aeiou".find(adj_1[0]) !=-1:  # first letter is vowel
        article = "An"
    else:
        article = "A"

    # create a poem

    poem = (
    f"{article} {adj_1} {n_1}\n\n"
    f"{article} {adj_1} {n_1} {v_1} {p_1} the {adj_2} {n_2}\n"
    f"{adv_1}, the {n_1} {v_2}\n"
    f"{n_2} {v_3} {p_2} a {adj_3} {n_3}"
)

    return poem


poem = make_poem()
print(poem)





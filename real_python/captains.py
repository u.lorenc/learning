# ex 1
captains = {}

# 2

captains["Enterprise"] = "Picard"
captains["Voyager"] = "Janeway"
captains["Defiant"] = "Sisko"


# ex 2


if "Enterprise" not in captains:
    captains["Enterprise"] = "unknown"
if "Discovery" not in captains:
    captains["Discovery"] = "unknown"

# for ship in ["Enterprise", "Discocery"]:
#   if not ship in captains:
#       captains[ship] = "unkown"


# ex 3

for ship, captain in captains.items():
    print(f"The {ship} is captained by {captain}.")


# ex 4

del captains["Discovery"]

# ex 5

captains = dict(
    [
        ("Enterprise", "Picard"),
        ("Voyager", "Janeway"),
        ("Defiant", "Sisko")
    ]
)

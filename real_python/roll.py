# ex1
# Write a function called roll() that uses randint() to simulate rolling
# a fair die by returning a random integer between 1 and 6.

from random import randint


def roll():
    return randint(1, 6)

# Write a program that simulates ten thousand rolls of a fair die and
# displays the average number rolled.


num_rolls = 10_000
total = 0

for trail in range(num_rolls):
    total = total + roll()

avg_roll = total / num_rolls
print(f"the average result of {num_rolls} rolls is {avg_roll}")

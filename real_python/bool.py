# logic statements
A = True # temperatures are high
B = True # artic ice is melting
C = True # sea levels are rising
D = False # all regions become hot


def follows(A, B):
    """ Returns True if 'A -> B'
    in words: 'B follows from A' """
    return not A or B

# do we have a climate change?
climate_change = follows(follows(A, B), C)
if not D:
    print(climate_change)
else:
    print(D)

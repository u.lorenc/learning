# Write a program called factors.py that asks the user to input a positive
# integer and then prints out the factors of that number

num = int(input("Enter a positive number: "))

for i in range(1, num + 1):
    if num % i == 0:
        print(f"{i} is a factor of {num}")




x = "Hello world"


def func():
    x = 2
    print(f"Inside 'func', x has the value {x}")


func()
print(f"Outside 'func', x has the value {x}")


total = 0


def add_to_total(n):
    global total  # niepoprawny sposób użycia funkcji global
    total = total + n


# add_to_total(5)
# print(total)
#
# x = 0
# while x < 10:
#     x = x + 1
# print(x)


n = 5
while n > 0:
    n = n - 1
    if n == 2:
        continue  # break 4,3, continue 4,3,1,0
    print(n)
print('Loop is finished')

# a = ['foo', 'bar', 'baz', 'qux', 'corge']
# while a:
#     if len(a) < 3:
#         continue
#     print(a.pop())
# print('Done.')

# a = ['foo', 'bar', 'baz', 'qux', 'corge']
# while a:
#     print(a.pop())
# else:
#     print('Done.')


# d = {'foo': 1, 'bar': 2, 'baz': 3}
# while len(d) > 3:
#     print(d.popitem())
# print('Done.')


# ex. 1

def cube(num):
    """ Return the cube of the input number."""
    cube_num = num ** 3 # could also use pow(num,3)
    return cube_num

print(f"8 cubed is {cube(8)}")
print(f"6 cubed is {cube(6)}")
    
# ex. 2

def greet(name):
    """ Dispaly a greeting """
    print(f"Hello {name}!")
greet("Guido")

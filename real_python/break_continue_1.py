# Using break, write a program that repeatedly asks the user for some
# input and quits only if the user enters "q" or "Q".
# infinite loop

while True:
    some_text = input('Type "q" or "Q" to quit: ')
    if some_text.upper() == "Q":
        break

# Using continue, write a program that loops over the numbers 1 to
# 50 and prints all numbers that are not multiples of 3.

for i in range(1, 51):
    if i % 3 == 0:
        continue
    print(i)

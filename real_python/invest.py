def invest(amount, rate, years):
    for year in range(1, years +1):
        amount = amount * (1 + rate)
        print(f"year {year}: ${amount:,.2f}")

amount = float(input("Enter the principal amount: "))
rate = float(input("Enter the annual rate: "))
years = int(input("Enter the numbers of years to calculate: "))


invest(amount, rate, years)


def multiply(x, y):
    """Return the product of two numbers x and y"""
    product = x * y
    return product


num = multiply(2, 4)
print(num)


def cube(x):
    """Return the cube of the input number."""
    power = x ** 3 # also pow(x, 3)
    return power


print(f"0 cubed is {cube(0)}")
print(f"2 cubed is {cube(2)}")


def greet(name):
    """Display a greeting."""
    print(f"Hello {name}!")


greet("Alex")

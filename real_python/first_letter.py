# 4.5 - Challenge: Pick Apart Your User's Input

# Return the upper-case first letter entered by the user

user_input = input("Tell me your password: ")
first_letter = user_input[0]
print("The first letter you entered was: ", first_letter.upper())

import random


def coin_flip():

    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

# First initialize the tallies to 0


heads_tally = 0
tails_tally = 0

for trail in range(50_000):
    if coin_flip() == "heads":
        heads_tally = heads_tally + 1
    else:
        tails_tally = tails_tally + 1

ratio = heads_tally/tails_tally
print(f"the ratio heads to tails is {ratio}")

# Exercise 1
# Ask the user to enter an integer.
# Repeat the process if the user hasn't entered an integer.

while True:

    try:
        my_input = int(input("Enter an integer: "))
        print(my_input)
        break
    except ValueError:
        print("Try again.")

